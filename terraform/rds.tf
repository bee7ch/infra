resource "aws_db_subnet_group" "exam-rds-subnet" {
  name = "dbsubnet"
  subnet_ids = [aws_subnet.exam-rds-subnet-public-1.id,
                aws_subnet.exam-rds-subnet-public-2.id, 
                aws_subnet.exam-rds-subnet-public-3.id]
  description = "use net 1,2,3"

  tags = {
    "Name" = "rds subnets"
  }
}

resource "aws_db_instance" "exam-rds6" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "exam"
  skip_final_snapshot  = true
  publicly_accessible  = true
  username             = "exam"
  password             = "exam_leroymerlin"
  parameter_group_name = "default.mysql5.7"
  db_subnet_group_name = aws_db_subnet_group.exam-rds-subnet.name

  vpc_security_group_ids = [aws_security_group.db-exam-rds-grp.id]
  
  tags = {
    "Name" = "exam-rds6"
  }
}
