resource "aws_vpc" "exam-rds-vpc" {
  cidr_block           = "10.31.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
  enable_classiclink   = false
  instance_tenancy     = "default"

  tags = {
    "Name" = "exam-rds-vpc"
  }
}

resource "aws_subnet" "exam-rds-subnet-public-1" {
  vpc_id                  = aws_vpc.exam-rds-vpc.id
  cidr_block              = "10.31.0.0/20"
  map_public_ip_on_launch = true
  availability_zone       = "eu-central-1a"

  tags = {
    "Name" = "exam-rds-subnet-public-1"
  }
}

resource "aws_subnet" "exam-rds-subnet-public-2" {
  vpc_id                  = aws_vpc.exam-rds-vpc.id
  cidr_block              = "10.31.16.0/20"
  map_public_ip_on_launch = true
  availability_zone       = "eu-central-1b"

  tags = {
    "Name" = "exam-rds-subnet-public-2"
  }
}

resource "aws_subnet" "exam-rds-subnet-public-3" {
  vpc_id                  = aws_vpc.exam-rds-vpc.id
  cidr_block              = "10.31.32.0/20"
  map_public_ip_on_launch = true
  availability_zone       = "eu-central-1c"

  tags = {
    "Name" = "exam-rds-subnet-public-3"
  }
}

resource "aws_internet_gateway" "exam-rds-igw" {
  vpc_id = aws_vpc.exam-rds-vpc.id

  tags = {
    "Name" = "exam-rds-igw"
  }
}

resource "aws_route_table" "exam-rds-public-crt" {
  vpc_id = aws_vpc.exam-rds-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.exam-rds-igw.id
  }

  route {
    cidr_block                = aws_vpc.exam-vpc.cidr_block
    vpc_peering_connection_id = aws_vpc_peering_connection.peering-ec2-rds.id
  }

  tags = {
    Name = "exam-rds-public-crt"
  }
}


resource "aws_route_table_association" "exam-rds-crta-public-subnet-1" {
  subnet_id      = aws_subnet.exam-rds-subnet-public-1.id
  route_table_id = aws_route_table.exam-rds-public-crt.id
}

resource "aws_route_table_association" "exam-rds-crta-public-subnet-2" {
  subnet_id      = aws_subnet.exam-rds-subnet-public-2.id
  route_table_id = aws_route_table.exam-rds-public-crt.id
}

resource "aws_route_table_association" "exam-rds-crta-public-subnet-3" {
  subnet_id      = aws_subnet.exam-rds-subnet-public-3.id
  route_table_id = aws_route_table.exam-rds-public-crt.id
}

resource "aws_security_group" "db-exam-rds-grp" {  
  name = "db security group"
  vpc_id = aws_vpc.exam-rds-vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = {
    "Name" = "db security group"
  }
}
