# need to peer between 
# resource "aws_vpc" "exam-rds-vpc"  cidr_block           = "10.31.0.0/16"
# resource "aws_vpc" "exam-vpc"      cidr_block           = "172.31.0.0/16"

resource "aws_vpc_peering_connection" "peering-ec2-rds" {
  #peer_owner_id = "${var.peer_owner_id}"
  peer_vpc_id   = aws_vpc.exam-vpc.id
  vpc_id        = aws_vpc.exam-rds-vpc.id
  auto_accept   = true
  tags = {
    Name = "VPC Peering RDS-EC2-instance"
  }
}














# resource "aws_route_table_association" "exam-rds-ec2-subnet-1" {
#   subnet_id      = aws_subnet.exam-rds-subnet-public-1.id
#   route_table_id = aws_route_table.rds-to-ec2.id
# }

# resource "aws_route_table_association" "exam-rds-ec2-subnet-2" {
#   subnet_id      = aws_subnet.exam-rds-subnet-public-2.id
#   route_table_id = aws_route_table.rds-to-ec2.id
# }

# resource "aws_route_table_association" "exam-rds-ec2-subnet-3" {
#   subnet_id      = aws_subnet.exam-rds-subnet-public-3.id
#   route_table_id = aws_route_table.rds-to-ec2.id
# }

# ##################

# resource "aws_route_table_association" "exam-instance-subnet-1" {
#   subnet_id      = aws_subnet.exam-rds-subnet-public-1.id
#   route_table_id = aws_route_table.ec2-to-rds.id
# }

# resource "aws_route_table_association" "exam-instance-subnet-2" {
#   subnet_id      = aws_subnet.exam-rds-subnet-public-2.id
#   route_table_id = aws_route_table.ec2-to-rds.id
# }

# resource "aws_route_table_association" "exam-instance-subnet-3" {
#   subnet_id      = aws_subnet.exam-rds-subnet-public-3.id
#   route_table_id = aws_route_table.ec2-to-rds.id
# }

